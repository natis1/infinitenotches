﻿/* Simple Hollow Knight Mod Base
 * Copyright (C) 2018  Eli Stone
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * For a copy of the GNU General Public License
 * see http://www.gnu.org/licenses/
 */

using System;
using System.Linq;
using System.Reflection;
using Modding;
// ReSharper disable MemberCanBePrivate.Global because these vars could be checked by other mods that wish to integrate with yours.
// ReSharper disable UnusedMember.Global because everything here is used implicitly.


namespace mymod
{
    // ReSharper disable once InconsistentNaming because mod api
    public class InfiniteNotches : Mod, ITogglableMod
    {
        public const string VERSION = "1.0";
        public const int LOAD_ORDER = 24;

        public override string GetVersion()
        {
            string ver = VERSION;
            // put version of modding api you are building against here. Probably 44.
            int minAPI = 40;
            bool apiTooLow = Convert.ToInt32(ModHooks.Instance.ModVersion.Split('-')[1]) < minAPI;
            
            // UNCOMMENT THIS SECTION IF YOU ARE USING ModCommon.
            /*
             
            bool noModCommon = true;
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly assembly in assemblies)
            {
                try
                {
                    if (assembly.GetTypes().All(type => type.Namespace != "ModCommon")) continue;
                    noModCommon = false;
                    break;
                }
                catch
                {
                    Log(assembly.FullName + " failed to load.");
                }
            }
            if (noModCommon) ver += " (Error: MODNAME requires ModCommon)";
            */
            
            if (apiTooLow) ver += " (Error: ModAPI too old)";
            Log("Version is " + ver);
            return ver;
        }
        
        public override void Initialize()
        {
            ModHooks.Instance.AfterSavegameLoadHook += SaveGame;
            ModHooks.Instance.NewGameHook += AddComponent;
            ModHooks.Instance.CharmUpdateHook += infiniteNotches;
            ModHooks.Instance.CharmUpdateHook -= finiteNotches;
        }

        private void infiniteNotches(PlayerData data, HeroController controller)
        {
            PlayerData.instance.charmSlots = 69420;
        }

        private void finiteNotches(PlayerData data, HeroController controller)
        {
            PlayerData.instance.charmSlots = 11;
        }

        private static void SaveGame(SaveGameData data)
        {
            AddComponent();
        }

        private static void AddComponent()
        {
            PlayerData.instance.charmSlots = 69420;
        }

        public override int LoadPriority()
        {
            return LOAD_ORDER;
        }

        public void Unload()
        {
            ModHooks.Instance.AfterSavegameLoadHook -= SaveGame;
            ModHooks.Instance.NewGameHook -= AddComponent;
            ModHooks.Instance.CharmUpdateHook -= infiniteNotches;
            ModHooks.Instance.CharmUpdateHook += finiteNotches;
        }
    }
}